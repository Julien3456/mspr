package org.example;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;

import static org.example.Database.createConnection;

public class Main {

    public static void main(String[] args) {

        // Connect to database
        Database database = createConnection();

        // Create session
        SparkSession sparkSession = SparkSession
                .builder().appName("mspr")
                .master("local")
                .getOrCreate();
        JavaSparkContext sparkContext = JavaSparkContext.fromSparkContext(sparkSession.sparkContext());

        // Collect Datas
        Dataset<Row> data = sparkSession
            .read()
            .format("csv")
            .option("header", "true")
            .option("delimiter", ",")
            .load("/home/julien/Documents/epsi/Cours/etl/MSPR/resultats-par-niveau-burvot-t2-france-entiere.csv");
        data.show(100);

        //Fiter datas
        Dataset<Row> datasetFilter = data
                .filter(
                        data.col("Code du département").isNotNull()
                                .and(data.col("Code de la commune").isNotNull())
                                .and(data.col("Abstentions").isNotNull())
                                .and(data.col("Votants").isNotNull())
                                .and(data.col("Nom").isNotNull())
                                .and(data.col("Voix").isNotNull())
                                .and(data.col("_c30").isNotNull())
                                .and(data.col("_c32").isNotNull()))
                .select(
                        data.col("Code du département"),
                        data.col("Code de la commune"),
                        data.col("Abstentions").cast("integer"),
                        data.col("Votants").cast("integer"),
                        data.col("Nom").as("Nom1"),
                        data.col("Voix").cast("integer").as("Voix1"),
                        data.col("_c30").as("Nom2"),
                        data.col("_c32").cast("integer").as("Voix2"));
        datasetFilter.show();

        //Aggreate datas
        Dataset<Row> avg_voix_dep_macr = datasetFilter.groupBy("Code du département", "Nom1")
                .avg("Voix1");
        Dataset<Row> avg_voix_dep_lepen = datasetFilter.groupBy("Code du département", "Nom2")
                .avg("Voix2");
        Dataset<Row> max_abs_dep = datasetFilter.groupBy("Code du département")
                .max("Abstentions");
        Dataset<Row> min_vot_com = datasetFilter.groupBy("Code de la commune")
                .min("Votants");

        //Save in DatawareHouse
        database.saveAvgMacron(avg_voix_dep_macr);
        database.saveAvgLepen(avg_voix_dep_lepen);
        database.saveMaxDep(max_abs_dep);
        database.saveMinCom(min_vot_com);
    }
}