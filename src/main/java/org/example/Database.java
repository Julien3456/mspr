package org.example;

import org.apache.hadoop.io.MapFile;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;

import java.util.Properties;

public class Database {
    private final Properties properties;
    private final String host = "localhost";
    private final String port = "5432";
    private final String database = "mspr";
    private final String jdbcUrl;

    public Database(Properties properties) {
        this.properties = properties;
        this.jdbcUrl = "jdbc:postgresql://" + this.host + ":" + this.port + "/" + this.database;
    }

    public Properties getProperties() {
        return properties;
    }

    public static Database createConnection(){
        String jdbcUrl = "jdbc:postgresql://localhost:5432/";
        String user = "julien";
        String password = "test";
        String jdbcDriver = "org.postgresql.Driver";

        Properties propConnection = new Properties();
        propConnection.put("user", user);
        propConnection.put("password", password);
        propConnection.put("jdbcUrl", jdbcUrl);
        propConnection.put("jdbcDriver", jdbcDriver);
        propConnection.put("dbname", "mspr");

        return new Database(propConnection);
    }

    private void saveDataset(String table, Dataset<Row> dataset) {
        dataset.write()
                .mode(SaveMode.Overwrite)
                .jdbc(this.jdbcUrl, table, this.properties);
    }

    public void saveAvgMacron(Dataset<Row> dataset){
        Dataset<Row> dfAvgVoix = dataset.select(
                dataset.col("Code du département"),
                dataset.col("Nom1"),
                dataset.col("avg(Voix1)"));
        saveDataset("avg_voix_macron", dataset);
    }

    public void saveAvgLepen(Dataset<Row> dataset){
        Dataset<Row> dfAvgVoix = dataset.select(
                dataset.col("Code du département"),
                dataset.col("Nom2"),
                dataset.col("avg(Voix2)"));
        saveDataset("avg_voix_lepen", dataset);
    }

    public void saveMaxDep(Dataset<Row> dataset){
        Dataset<Row> dfAvgVoix = dataset.select(
                dataset.col("Code du département"),
                dataset.col("max(Abstentions)"));
        saveDataset("MaxDep", dataset);
    }

    public void saveMinCom(Dataset<Row> dataset){
        Dataset<Row> dfAvgVoix = dataset.select(
                dataset.col("Code de la commune"),
                dataset.col("min(Votants)"));
        saveDataset("MinCom", dataset);
    }
}
